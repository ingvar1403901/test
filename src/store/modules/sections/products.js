export default {
    state: {
        data: [],
        favoritesIds: [],
        basketsIds: [],
        pagination: {
            dotsCurrentIndex: 0,
            dotsLength: 0,
            productsShowLength: 0,
            productsShowFirst: 0,
            productsShowLast: 0,
        },
        dataById: undefined,
        search: "",
        sort: {
            by: "title",
            status: true
        },
        filter: {
            by: "Микс",
            list: ["Микс","Фильмы","Мультфильмы","Сериалы"]
        }
    },
    actions: {
        async actionData(ctx) {
            let res = await fetch("http://myjson.dit.upm.es/api/bins/308f", {});
            const productsArray = await res.json();
            ctx.commit('mutateProductsData', productsArray);
        },
        actionPagination(ctx, paginationCurrentIndex) {
            let productsShowLength;
            if (paginationCurrentIndex === undefined) {
                paginationCurrentIndex = 0;
                productsShowLength = 4;
            }
            ctx.commit('mutatePagination', {
                paginationCurrentIndex,
                productsShowLength,
            });
        },
        actionSearch(ctx, value) {
            ctx.commit('mutateSearch', value);
        },
        actionDataById(ctx, id) {
            ctx.commit('mutateDataById', id);
        },
        actionSort(ctx, value) {
            ctx.commit('mutateSort', value);
        },
        actionFilter(ctx, value) {
            ctx.commit('mutateFilter', value);
        },
        actionFavorite(ctx, id) {
            if (id === undefined) id = [];
            ctx.commit('mutateFavorites', id);
        },
        actionBaskets(ctx, id) {
            if (id === undefined) id = [];
            ctx.commit('mutateBaskets', id);
        },
    },
    mutations: {
        mutateProductsData(state, productsArray) {
            state.data = productsArray;
        },
        mutatePagination(state, { paginationCurrentIndex, productsShowLength }) {
            state.pagination.dotsCurrentIndex = paginationCurrentIndex + 1;
            if (productsShowLength !== undefined) state.pagination.productsShowLength = productsShowLength;
            state.pagination.productsShowFirst = state.pagination.dotsCurrentIndex * state.pagination.productsShowLength - state.pagination.productsShowLength;
            state.pagination.productsShowLast = state.pagination.dotsCurrentIndex * state.pagination.productsShowLength;
        },
        mutateDataById(state, id) {
            state.dataById = Number(id);
        },
        mutateFavorites(state, id) {
            if(Array.isArray(id)){ // данные с сервера
                state.favoritesIds = state.favoritesIds.concat(id);
            }
            else{ // измененеие данных
                let index = state.favoritesIds.indexOf(id);
                if (index !== -1) {
                    state.favoritesIds.splice(index, 1);
                }else{
                    state.favoritesIds.push(id);
                }
            }
        },
        mutateBaskets(state, id) {
            if(Array.isArray(id)){ // данные с сервера
                state.basketsIds = state.basketsIds.concat(id);
            }else { // измененеие данных
                let index = state.basketsIds.indexOf(id);
                if (index !== -1) {
                    state.basketsIds.splice(index, 1);
                } else {
                    state.basketsIds.push(id);
                }
            }
        },
        mutateSort(state, value) {
            state.sort.by = value;
            state.sort.status = !state.sort.status;
        },
        mutateFilter(state, value) {
            state.filter.by = value;
        },
        mutateSearch(state, value) {
            state.search = value;
        },
    },
    getters: {
        // получение данных
        getData(state) {
            return state.data;
        },
        // получение данных пагинации
        getPaginationSettings(state) {
            return state.pagination;
        },
        // получение данных по товаром отфильтроанным сортировке
        getProductsFilter(state) {
            return state.data.filter(t => t.category === (state.filter.by !== "Микс" ? state.filter.by : t.category)).sort((a, b) => state.sort.status? a[state.sort.by] > b[state.sort.by] ? 1 : -1 : a[state.sort.by] < b[state.sort.by] ? 1 : -1);
        },
        // получение данных по товаром отфильтроанным с пагинацией
        getProductsPaginationFilter(state) {
            return state.data.filter(t => t.category === (state.filter.by !== "Микс" ? state.filter.by : t.category)).sort((a, b) => state.sort.status? a[state.sort.by] > b[state.sort.by] ? 1 : -1 : a[state.sort.by] < b[state.sort.by] ? 1 : -1).slice(state.pagination.productsShowFirst, state.pagination.productsShowLast);
        },
        // получение данных по товарам отфильтрованным по избранному
        getProductsFavoriteFilter(state) {
            return state.data.filter((t) => state.favoritesIds.includes(t.id));
        },
        // получение данных по товарам отфильтрованным по добавлению в корзину
        getProductsBasketFilter(state) {
            return state.data.filter((t) => state.basketsIds.includes(t.id));
        },
        // получение данных по товарам отфильтрованным по поиску
        getProductSearchFilter(state) {
            return state.data.filter(t => {
                if (state.search !== "") {
                    return t.title.toLowerCase().includes(state.search.toLowerCase());
                }
            });
        },
        // получение данных по товарам добавленным в избранное
        getFavorite(state) {
            return state.favoritesIds;
        },
        // получение данных по товарам добавленным в корзину
        getBaskets(state) {
            return state.basketsIds;
        },
        // получение данных товарам отфильтрованным по id
        getDataById(state) {
            if(state.dataById)return state.data.find(t => t.id === state.dataById);
            else return false;
        },
        // получение сортировок
        getSort(state) {
            return state.sort;
        },
        // получение фильтраций
        getFilter(state) {
            return state.filter;
        },
    },
};