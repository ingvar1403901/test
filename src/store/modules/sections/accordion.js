export default {
    state: {
        accordionData: [],
        accordionCurrent: false
    },
    actions: {
        // accordion ACTIONS
        async actionAccordionData(ctx) {
            let res = await fetch("https://jsonplaceholder.typicode.com/posts?_limit=5", {
            });
            const accordionArray = await res.json();
            ctx.commit("mutateAccordionData", accordionArray);
        },
        actionAccordionIndex(ctx, index) {
            ctx.commit("mutateAccordionIndex", index);
        },
    },
    mutations: {
        // accordion MUTATIONS
        mutateAccordionData(state, accordionArray) {
            state.accordionData = accordionArray;
        },
        mutateAccordionIndex(state, index) {
            state.accordionCurrent !== index ? state.accordionCurrent = index : state.accordionCurrent = false;
        },
    },
    getters: {
        // accordion GETTERS
        getAccordionData(state) {
            return state.accordionData
        },
        getAccordionIndex(state) {
            return state.accordionCurrent
        },
    },
};