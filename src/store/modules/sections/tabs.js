export default {
    state: {
        tabsData: [],
        tabsCurrent: 0
    },
    actions: {
        // tabs ACTIONS
        async actionTabData(ctx) {
            let res = await fetch("https://jsonplaceholder.typicode.com/posts?_limit=3", {
            });
            const tabsArray = await res.json();
            ctx.commit("mutateTabData", tabsArray);
        },
        actionTabIndex(ctx, index) {
            ctx.commit("mutateTabIndex", index);
        },
    },
    mutations: {
        // tabs MUTATIONS
        mutateTabData(state, tabsArray) {
            state.tabsData = tabsArray;
        },
        mutateTabIndex(state, index) {
            state.tabsCurrent = index;
        },
    },
    getters: {
        // tabs GETTERS
        getTabData(state) {
            return state.tabsData
        },
        getTabIndex(state) {
            return state.tabsCurrent
        },
    },
};