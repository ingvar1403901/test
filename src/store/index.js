import { createStore } from 'vuex'
import products from "@/store/modules/sections/products";
import accordion from "@/store/modules/sections/accordion";
import tabs from "@/store/modules/sections/tabs";
export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    products,
    accordion,
    tabs,
  }
})
