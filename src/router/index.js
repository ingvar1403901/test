import { createRouter, createWebHistory } from 'vue-router'
import Base from '../views/Base.vue'
import Home from '../views/Home.vue'
import Cart from '../views/Cart.vue'
import Favorites from '../views/Favorites.vue'
import Search from '../views/Search.vue'
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/base',
    name: 'Base',
    component: Base
  },
  {
    path: '/favorites',
    name: 'Favorites',
    component: Favorites
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart
  },
  {
    path: '/search',
    name: 'Search',
    component: Search
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router
